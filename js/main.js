// получаем все иконки
const passwordIcon = document.querySelectorAll(".icon-password");

//цыклем по иконкам и добавляем клик, получаем инпут
passwordIcon.forEach(function (icon) {
  icon.addEventListener("click", function () {
    const inputField = icon.parentElement.querySelector("input");
// делаем проверку виден ли пароль
    if (inputField.type === "password") {
      inputField.type = "text";
      icon.classList.remove("fa-eye");
      icon.classList.add("fa-eye-slash");
      const nextSibling = icon.nextElementSibling;
      if (nextSibling) {
        nextSibling.textContent = "Скрыть пароль";
      }
    } else {
      inputField.type = "password";
      icon.classList.remove("fa-eye-slash");
      icon.classList.add("fa-eye");
      const nextSibling = icon.nextElementSibling;
      if (nextSibling) {
        nextSibling.textContent = "Паказать пароль";
      }
    }
  });
});

// получаем кнопку (подтвердить)
const submitBtn = document.querySelector(".btn");

// добавляем клик
submitBtn.addEventListener("click", function (event) {
  event.preventDefault();

// получаем два поля ввода пароля
  const passwordInputTwo = document.querySelectorAll("input[type='password']");

// проверяем совпадают ли введённые пароли
  if (passwordInputTwo[0].value === passwordInputTwo[1].value) {
    alert("You are welcome");
  } else {
    const errorText = document.createElement("span");
    errorText.textContent = "Потрібно ввести однакові значення";
    errorText.style.color = "red";
    submitBtn.parentElement.insertBefore(errorText, submitBtn);
  }
});